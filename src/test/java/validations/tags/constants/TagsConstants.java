package validations.tags.constants;

import org.openqa.selenium.By;

public interface TagsConstants {
        By TAGS_TITLE = By.xpath("//h2[normalize-space()='Tags']");
        By TAGS_LIST = By.cssSelector(".datatable-wrapper datatable-container table tbody tr");
        By CREATE_TAG_BUTTON = By.xpath("//a[normalize-space()='Add Tag']");
        By ADD_TAG_TITLE  = By.xpath("//h2[normalize-space()='Add Tag']");
        By CREATE_TAG_INPUT = By.xpath("//input[@id='name']");
        By CREATE_TAG_SUBMIT_BUTTON = By.xpath("//button[normalize-space()='Submit']");
        By VALIDATE_NEW_CREATED_TAG_NAME = By.xpath("//table[@id='datatablesSimple']//tbody//tr[1]//td[2]");
        By SUCCESSFULLY_CREATED_TAG_ALERT = By.xpath("//div[@class='alert alert-success alert-dismissible fade show " +
                "mt-4']");

        By DISMISS_ALERT = By.xpath("//button[@data-bs-dismiss='alert']");

        
}
