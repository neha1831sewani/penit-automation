package validations.tags;

import com.github.javafaker.Faker;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import validations.tags.constants.TagsConstants;


import java.time.Duration;
import java.util.HashMap;

public class TagsValidation implements TagsConstants {

    WebDriver driver;
    HashMap<String,Object> tags;
    private Faker faker;
    WebDriverWait wait;


    public  TagsValidation(WebDriver driver) {
        this.driver = driver;
        faker = new Faker();
        wait = new WebDriverWait(driver, Duration.ofSeconds(20));

    }

    public void createAndValidateTag() throws InterruptedException {
        WebElement addTagButton  = driver.findElement(CREATE_TAG_BUTTON);
        addTagButton.click();
        tags = new HashMap<>();
        System.out.println("Called");

        Thread.sleep(3000);
        String addTagTitle = (driver.findElement(ADD_TAG_TITLE)).getText();
        System.out.println(addTagTitle);

        Assert.assertEquals(addTagTitle, "Add Tag", "Title mismatched! Landed on wrong page!");
        System.out.println("reached forinput");

        Thread.sleep(3000);
        String fakeTag = faker.lorem().word();
        System.out.println("fake tag: " + fakeTag);

        tags.put("tag_name", fakeTag);
        WebElement tagInput = driver.findElement(CREATE_TAG_INPUT);
        tagInput.sendKeys(fakeTag);

        WebElement submitButton = driver.findElement(CREATE_TAG_SUBMIT_BUTTON);
        submitButton.click();

        WebElement successfullyCreatedTagAlertMessage = driver.findElement(SUCCESSFULLY_CREATED_TAG_ALERT);
        Assert.assertTrue(successfullyCreatedTagAlertMessage.isDisplayed(), "Tag not created");
        WebElement dismissAlert = driver.findElement(DISMISS_ALERT);
        dismissAlert.click();
        Thread.sleep(3000);

        String validateTagName = driver.findElement(VALIDATE_NEW_CREATED_TAG_NAME).getText();
        Assert.assertEquals(validateTagName, tags.get("tag_name"), "New tag not added to list");
    }

}
 