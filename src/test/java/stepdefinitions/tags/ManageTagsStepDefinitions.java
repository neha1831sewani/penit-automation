package stepdefinitions.tags;

import factory.DriverFactory;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import tests.tags.TagsTest;

public class ManageTagsStepDefinitions {


    WebDriver driver;
    TagsTest tagsTest;

    public ManageTagsStepDefinitions(){
        this.driver = DriverFactory.getDriver();
        tagsTest = new TagsTest(driver);

    }
    @Given("I am on the tags page")
    public void iAmOnTheTagsPage(){
        driver.get("http://127.0.0.1:8000/tags");
    }

    @Then("I clicked Add Tag button and created a new tag")
    public void  i_clicked_add_tag_button_and_created_a_new_tag() throws InterruptedException {
        tagsTest.createAndValidateTag();

    }

}
