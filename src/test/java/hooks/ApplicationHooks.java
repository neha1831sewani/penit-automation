package hooks;

import factory.DriverFactory;

import io.cucumber.java.After;
import io.cucumber.java.Before;

import java.net.MalformedURLException;

public class ApplicationHooks {
  DriverFactory driverFactory = new DriverFactory();

    @Before
    public void openBrowser() throws MalformedURLException {
         driverFactory.initDriver();
    }

    @After
    public void tearDown(){
//        DriverFactory.getDriver().quit();
    }

}
