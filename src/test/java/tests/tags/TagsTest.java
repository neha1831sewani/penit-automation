package tests.tags;

import org.openqa.selenium.WebDriver;
import validations.tags.TagsValidation;

public class TagsTest {

    WebDriver driver;
   TagsValidation tagsValidation;

    public TagsTest(WebDriver driver){
        this.driver = driver;
       tagsValidation =new TagsValidation(driver);


    }

    public void createAndValidateTag() throws InterruptedException {
        tagsValidation.createAndValidateTag();
    }

}
