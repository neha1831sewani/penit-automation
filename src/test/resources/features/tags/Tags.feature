@Tags
  Feature: Managing Tags
   In order to manage tags effectively
    I want to be able to create, read, update, and delete tags
  Background: User is on tags page
#    Given I visited login page
#    Then I logged in with "user19@gmail.com" username and "abcd1234" password
    Scenario: Viewing the list of tags
      Given I am on the tags page
      Then I clicked Add Tag button and created a new tag